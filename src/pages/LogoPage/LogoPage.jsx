import { useEffect, useState } from 'react';
import { Redirect } from 'react-router-dom';
import './LogoPage.scss';
import logo from '../../assets/images/img.png';


const LogoPage = () => {
    const [redirectNow, setRedirectNow] = useState(false);

    useEffect(() => {
        setTimeout(() => {
          setRedirectNow(!redirectNow);
        }, 1000);
    }, []);

    if (redirectNow) {
        return <Redirect to='/loading' />
    }

    return (
        <div className="page">
            <div className="page__logo">
                {/* <img src={logo} alt="logo" className="logo"/> */}
            </div>
        </div>
    )
}

export default LogoPage;