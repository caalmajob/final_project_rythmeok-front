import { useEffect, useState } from 'react';
import { useSelector } from 'react-redux';
import { Redirect } from 'react-router-dom';
import './LoadingPage.scss';
// import waves from '../../assets/images/soundWaves.png';

const LoadingPage = () => {
    const [redirectNow, setRedirectNow] = useState(false);
    const {user} = useSelector(state => state.user);

    useEffect(() => {
        setTimeout(() => {
            setRedirectNow(!redirectNow);
        }, 2000);
    }, []);

    if (redirectNow) {
        if (!user) {
            return <Redirect to='/auth' />
        }
        return <Redirect to='/home' />
    }


    return(
        <div className="loading">
            <div className="loading__wave">
                {/* <img src={`${waves}`} alt="sound wave" className="waves"/> */}
                <div className="stroke"></div>
                <div className="stroke"></div>
                <div className="stroke"></div>
                <div className="stroke"></div>
                <div className="stroke"></div>
                <div className="stroke"></div>
                <div className="stroke"></div>
                <div className="stroke"></div>
                <div className="stroke"></div>
                <div className="stroke"></div>
                <div className="stroke"></div>
                <div className="stroke"></div>
                <div className="stroke"></div>
                <div className="stroke"></div>
                <div className="stroke"></div>
                <div className="stroke"></div>
                <div className="stroke"></div>
                <div className="stroke"></div>
                <div className="stroke"></div>
                <div className="stroke"></div>
                <div className="stroke"></div>
                <div className="stroke"></div>
                <div className="stroke"></div>
                <div className="stroke"></div>
                <div className="stroke"></div>
                <div className="stroke"></div>
                <div className="stroke"></div>
                <div className="stroke"></div>
                <div className="stroke"></div>
                <div className="stroke"></div>
            </div>
        </div>
    )
}

export default LoadingPage;