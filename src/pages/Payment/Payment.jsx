import { useState } from 'react';
import { Redirect, useLocation } from 'react-router-dom';
import { useSelector } from 'react-redux';
import { buyConcert } from '../../api/concert.api';
import './Payment.scss';

const INITIAL_STATE = {
    id: "",
    userId: "",
};

const Payment = () => {
    const location = useLocation();
    const { concertInfo } = location.state;
    const [form, setForm] = useState(INITIAL_STATE);
    const [redirectNow, setRedirectNow] = useState(false);
    const { user } = useSelector(state => state.user);


    const submit = (ev) => {
        ev.preventDefault();
        buyConcert(form);
        setForm(INITIAL_STATE);
        setRedirectNow(!redirectNow);    
     }


     if (redirectNow) {
        return <Redirect to='/home' />
    }

    const inputChange = ev => {
        const { name, value } = ev.target;
        setForm({ ...form, [name]: value });
    };
    
    return (
        <div className="payment">
            <div className="payment__container">
                <div className="payment__title" >
                    <p>Compra tus entradas</p>
                </div>
                <div className="payment__form">
                    <form >
                        <div>
                            <p className="payment__form-ticket">Entrada General: { }</p>
                            <select className="payment__form-input">
                                <option value="1">1 Entrada</option>
                                <option value="2" selected>2 Entradas</option>
                                <option value="3">3 Entradas</option>
                            </select>
                            <p className="payment__form-ticket">Total: { }</p>
                        </div>
                        <div>
                            <p className="payment__form-title">Datos del comprador</p>
                            <input className="payment__form-input"
                                type="text"
                                name="name"
                                id="name"
                                placeholder="Nombre y Apellidos"
                            />
                            <input className="payment__form-input"
                                type="email"
                                name="email"
                                id="email"
                                placeholder="Email"
                            />
                            <input className="payment__form-input"
                                type="number"
                                name="code"
                                id="code"
                                placeholder="Código Postal"
                            />
                        </div>
                        <div>
                            <p className="payment__form-title">Pago con tarjeta</p>
                            <input className="payment__form-input"
                                type="number"
                                name="card"
                                id="card"
                                placeholder="Nº de Tarjeta"
                            />
                            <input className="payment__form-input payment__form-small"
                                type="text"
                                name="date"
                                id="date"
                                placeholder="mes / año"
                            />
                            <input className="payment__form-input payment__form-small"
                                type="number"
                                name="cvv"
                                id="cvv"
                                placeholder="CVV"
                            />
                            <input className="payment__form-input"
                                type="text"
                                name="title"
                                id="title"
                                placeholder="Titular"
                            />
                        </div>
                    </form>
                    <div>
                        <form onSubmit={submit} encType="multipart/form-data" method="POST">

                            <input type="hidden" name="id" value={form.id = concertInfo.concertlist._id } onChange={inputChange} />

                            <input type="hidden" name="userId" value={form.userId = user._id} onChange={inputChange} />

                            <button className="payment__form-button" type="submit">Pagar</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default Payment;