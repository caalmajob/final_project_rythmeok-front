import { useEffect } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { Link } from 'react-router-dom';

import { Gallery, Button } from '../../components';
import { getAllConcertHallAsync } from '../../redux/slices/concertHall.slice';

import './Home.scss';


const Home = (props) => {
    const {user} = useSelector(state=>state.user);
    const concertHalls = useSelector(state=>state.concertHalls.all);
    const concerts = useSelector(state=>state.concerts.all);
    let dataArtist = [];
    let objectArtist = {}
    let dataHall = [];
    let objectHall = {}
    const STYLES = [
        {
            name: 'ROCK',
            url: 'http://pngimg.com/uploads/rock_music/rock_music_PNG40.png',
        },
        {
            name: 'POP',
            url:
                'https://pngimage.net/wp-content/uploads/2018/06/pop-music-png-6.png',
        },
        {
            name: 'R&B',
            url:
                'https://pm1.narvii.com/6665/b1b20273e19568046b5cb79d50f18e97eee2d400_hq.jpg',
        },
        {
            name: 'HIP-HOP',
            url:
                'https://png.pngtree.com/png-clipart/20191128/ourmid/pngtree-hiphop-hip-hop-colorful-graffiti-font-effect-png-image_86181.jpg',
        },
        { name: 'JAZZ', url: 'https://salamancartvaldia.es/adjuntos/Jazz.jpg' },
        {
            name: 'TECHNO',
            url:
                'https://previews.123rf.com/images/stuartphoto/stuartphoto1509/stuartphoto150900676/45131069-tecno-m%C3%BAsica-representando-jazz-el%C3%A9ctrico-y-detroit.jpg',
        },
        { name: 'TRAP', url: 'https://www.ecured.cu/images/e/e7/TRAP-MUSIC.jpg' },
    ];
    
    concerts.map(concert=>{
        objectArtist={...objectArtist, image:concert.image, url: concert.spotifyUrl, name: concert.artist};
        dataArtist = [...dataArtist, objectArtist];
    })
    concertHalls.map(hall=>{
        objectHall={...objectHall, image: hall.logo, url: 'urlConcertHall', name: hall.name};
        dataHall = [...dataHall, objectHall]
    })

    const dispatch = useDispatch()
    

    
    useEffect(() => {
        dispatch(getAllConcertHallAsync());
    }, [])

    return (
        <div className="home">
            <div className="home__button">
                <Link to={{pathname :"/chat"}}><Button message="Chat" bgColor="#01918E" textColor="#fff"/></Link>
                <Button message="Amigos" bgColor="#01918E" textColor="#fff"/>
                <Button message="Fan Club" bgColor="#01918E" textColor="#fff"/>
            </div>
            <div className="home__title">
                { user && <h1 className="home__title-name">{`¡Hola ${user.name}!`}</h1>}
                <p className="home__title-info">¿Qué concierto te apetece?</p>
            </div>
            <div className="home__gallery">
                <div>
                    <p>Artistas</p>
                    <Gallery dataArtist={dataArtist}/>
                </div>
                <div>
                    <p>Estilos</p>
                    <Gallery styles={STYLES}/>
                </div>
                <div>
                    <p>Salas</p>
                    <Gallery dataHall={dataHall}/>
                </div>
                
            </div>
        </div>
    )
}

export default Home;