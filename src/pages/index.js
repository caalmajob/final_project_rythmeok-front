import LoadingPage from './LoadingPage/LoadingPage';
import LogoPage from '../pages/LogoPage/LogoPage';
import Home from './Home/Home';
import Tickets from './Tickets/Tickets';
import MusicPlatforms from './MusicPlatforms/MusicPlatforms';
import AuthPage from './AuthPage/AuthPage';
import Payment from './Payment/Payment';
import UserConfig from './UserConfig/UserConfig';
import ConcertDetail from './ConcertDetail/ConcertDetail';
import Chat from './Chat/Chat';
import Concerts from './Concerts/Concerts';
import TicketDetail from './TicketDetail/TicketDetail';
import SearchConcert from './SearchConcert/SearchConcert';


export {
    LoadingPage,
    Home,
    Tickets,
    LogoPage,
    MusicPlatforms,
    AuthPage,
    Payment,
    UserConfig,
    ConcertDetail,
    Chat,
    Concerts,
    TicketDetail,
    SearchConcert,
}
