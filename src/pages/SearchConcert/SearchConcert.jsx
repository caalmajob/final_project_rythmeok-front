import { useEffect, useState } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { getAllConcertsAsync } from '../../redux/slices/concert.slice';
import { ConcertList } from '../../components';

import './SearchConcert.scss';

const SearchConcert = () => {
    const concerts = useSelector(state => state.concerts.all);
    const [searchInput, setSearchInput] = useState('');
    const dispatch = useDispatch();

    useEffect(() => {
        if (!concerts.length) dispatch(getAllConcertsAsync());;
    }, []);

    const changeSearchInput = (search) => {
        setSearchInput(search.toLowerCase());
    };

    const filterConcerts = () => {
        if(searchInput){
            return concerts.filter(t => t.artist.toLowerCase().includes(searchInput));
        } else {
            return concerts;
        }
    };

    return (
        <div className="search__container">
            <div className="search">
                <h1 className="search__title">¡Busca tu artista favorito!</h1>
                <input
                    className="search__input"
                    type="text"
                    placeholder="Buscar"
                    onChange={(ev)=> changeSearchInput(ev.target.value)}
                    value={searchInput}
                />
            </div>
            <ul className="tickets__list">
                {filterConcerts() && filterConcerts().map((el, index) => {
                    return (
                        <li className="tickets__list" key={`${el}-${index}`}>
                            <ConcertList concertlist={el} />
                        </li>
                    );
                })}
            </ul>
        </div>
    );
};

export default SearchConcert;