import './MusicPlatforms.scss';
import tidal from '../../assets/images/tidal.png';
import spotify from '../../assets/images/spotify.jpg';
import apple from '../../assets/images/apple.png';
import deezer from '../../assets/images/deezer.png';
import youtube from '../../assets/images/youtube.png';
import google from '../../assets/images/google.jpg';
import amazon from '../../assets/images/amazon.jpg';
import sound from '../../assets/images/sound.jpg';



const MusicPlatforms = () => {

    return (
        <div className="music">
            <p className="music__title">SINCRONIZA TU MÚSICA Y TUS PLATAFORMAS</p>
            <div className="music__main">
                <div className="music__container">
                    <button className="music__platform"><img src={spotify} alt="spotify" className="music__logo"/></button>
                    <button className="music__platform"><img src={apple} alt="apple" className="music__logo"/></button>
                    <button className="music__platform"><img src={deezer} alt="deezer" className="music__logo"/></button>
                    <button className="music__platform"><img src={tidal} alt="tidal" className="music__logo"/></button>
                    <button className="music__platform"><img src={youtube} alt="youtube" className="music__logo"/></button>
                    <button className="music__platform"><img src={google} alt="google" className="music__logo"/></button>
                    <button className="music__platform"><img src={amazon} alt="amazon" className="music__logo"/></button>
                    <button className="music__platform"><img src={sound} alt="sound" className="music__logo"/></button>
                </div>
            </div>
        </div>
    )
}

export default MusicPlatforms;