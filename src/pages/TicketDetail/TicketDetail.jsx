
import { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';

import './TicketDetail.scss';
import euro from '../../assets/icons/euro.png'
import clock from '../../assets/icons/clock.png';
import qr from '../../assets/images/qr.png';


const TicketDetail = () => {
    const { tickets } = useSelector(state => state.user.user);

    return (
        <>

            <div className="ticketdetail">
                <ul >
                    {tickets.map((el, index) => {
                        const date = new Date(el.date);
                        const formatDate = date.toLocaleDateString();
                        return (
                            <li className="concerts__list" key={`${el}-${index}`}>
                                <div className="ticketdetail__main">
                                    <div className="ticketdetail__image">
                                        <img className="ticketdetail__images" src={el.image} alt="ticket" />
                                    </div>
                                    <div className="ticketdetail__info">
                                        <p className="ticketdetail__info-artist">{el.artist}</p>
                                        <p><img src={clock} alt="time" srcset="" className="card__icon"/>{formatDate}</p>
                                        <p><img src={euro} alt="price" srcset="" className="card__icon"/>{el.price}</p>
                                    </div>
                                </div>
                                <div className="ticketdetail__second">
                                <p className="ticketdetail__second-info">{el.concertName}</p>
                                <p className="ticketdetail__secondf"> en {el.concertHall.name}</p>
                                <p className="ticketdetail__secondf">{el.location}</p>
                                <p className="ticketdetail__secondf"> Apertura de puertas 21:00</p> 
                                <img src={qr} alt="time" />
                                </div>

                            </li>);
                    })}
                </ul>
            </div>
        </>
    )
}


export default TicketDetail;