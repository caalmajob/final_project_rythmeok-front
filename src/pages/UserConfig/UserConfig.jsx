import { useState } from 'react';
import { useDispatch } from 'react-redux';
import { logoutAsync } from '../../redux/slices/user.slice';
import { Link, Redirect } from "react-router-dom";
import './UserConfig.scss';


const UserConfig = () => {
    const [loggedOut, setLoggedOut] = useState(false);
    const dispatch = useDispatch();

    const logoutUser = () => {
        dispatch(logoutAsync());
        setLoggedOut(!loggedOut);
    }

    if (loggedOut) {
        return <Redirect to='/auth' />
    }
    return (
        <div className="config">
            <div className="config__container">
                <p className="config__container-title">PERSONAL</p>
                <button className="config__button"><Link to={{ pathname: "/profile" }}>Editar perfil</Link></button>
                <button className="config__button"><Link to={{ pathname: "/ticketdetail" }}>Historial de Compras</Link></button>
                <button className="config__button"><Link to={{ pathname: "/platforms" }}>Sincroniza tu música</Link></button>
                <button onClick={logoutUser} className="config__button" >Cerrar Sesión</button>
            </div>
        </div>
    )
}

export default UserConfig;