import { useSelector, useDispatch } from 'react-redux';
import { useState } from 'react';
import { useLocation, Link } from 'react-router-dom';
import { wishlistConcert } from '../../api/concert.api';

import { Button } from '../../components';


import './ConcertDetail.scss';
import euro from '../../assets/icons/euro.png'
import clock from '../../assets/icons/clock.png';
import play from '../../assets/icons/play.png';

const INITIAL_STATE = {
    id: "",
    userId: "",
};

const ConcertDetail = (props) => {
    const user = useSelector(state => state.user);
    const [form, setForm] = useState(INITIAL_STATE);
    const [clicked, setClicked] = useState(false);
    const dispatch = useDispatch()
    const location = useLocation();
    const { concertInfo } = location.state;
    const date = new Date(concertInfo.concertlist.date);
    const formatDate = date.toLocaleDateString()

    const submit = (ev) => {
        ev.preventDefault();
        wishlistConcert(form);
        setForm(INITIAL_STATE);
        setClicked(!clicked);
    }

    const inputChange = ev => {
        const { name, value } = ev.target;
        setForm({ ...form, [name]: value });
    };

    return (
        <div>
            <div className="wrapper">
                <div className="card">
                    <div className="card__image-container"><img src={concertInfo.concertlist.image} alt={concertInfo.concertlist.artist} srcset="" className="card__image" /></div>
                    <div className="card__info">
                        <div className="card__title">{concertInfo.concertlist.artist}</div>
                        <div className="card__date">{formatDate}</div>
                        <div>
                            <div className="card__time"><span className="icon__container"><img src={clock} alt="time" srcset="" className="card__icon" /></span>22:00</div>
                            <div className="card__price"><span className="icon__container"><img src={euro} alt="price" srcset="" className="card__icon" /></span>{concertInfo.concertlist.price}</div>
                        </div>
                    </div>

                </div>
                <div className="card__iframe">
                    <iframe
                        className="search__iframe"
                        src={concertInfo.concertlist.spotifyUrl}
                        width="300"
                        height="80"
                        frameborder="0"
                        allowtransparency="true"
                        allow="encrypted-media">
                    </iframe>
                </div>
                <div className="buttons__container">
                    <Link to={{ pathname: "/payment", state: { concertInfo } }}><Button message="Comprar" bgColor="#FFDC35" textColor="black"></Button></Link>
                    <form onSubmit={submit} encType="multipart/form-data" method="POST">

                        <input type="hidden" name="id" value={form.id = concertInfo.concertlist._id} onChange={inputChange} />
                        <input type="hidden" name="userId" value={form.userId = user.user._id} onChange={inputChange} />
                        {clicked && <Button message="+ Deseos" bgColor="#4DD599" textColor="#fff" type="submit"></Button>}
                        {!clicked && <Button message="+ Deseos" bgColor="#01918E" textColor="#fff" type="submit"></Button>}
                    </form>
                </div>
                <div>
                    <span className="icon"></span>
                    <p className="white-text">SALA CLAMORES - MADRID, ESPAÑA</p>
                </div>
                <div>
                    <span className="icon"></span>
                    <p className="white-text">{concertInfo.concertlist.concertName}</p>
                </div>
                <div>
                    <span className="icon"></span>
                    <p className="white-text">{concertInfo.concertlist.styles}</p>
                    <p className="white-text">{concertInfo.concertlist.attendees.length} asistirán</p>
                    <p className="white-text">A {concertInfo.concertlist.likes.length} les gusta</p>
                </div>
                <div className=" white-text">
                    {concertInfo.concertlist.detailConcert}

                </div>


            </div>
        </div>
    )
}

export default ConcertDetail;