import { useEffect, useState } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { Button } from '../../components/';
import { getAllChatsAsync, addMessageAsync } from '../../redux/slices/chat.slice';
import { addMessage } from '../../api/chat.api';
import './Chat.scss';
import profile from '../../assets/images/user.png';

const INITIAL_STATE = {
    id: "",
    message: "",
};


const Chat = () => {
    const chats = useSelector(state => state.chats.all);
    const { user } = useSelector(state => state.user);
    const dispatch = useDispatch();

    useEffect(() => {
        if (!chats.length) dispatch(getAllChatsAsync());
    }, []);


    const [form, setForm] = useState(INITIAL_STATE);

    const submit = (ev) => {
        ev.preventDefault();
        dispatch(addMessageAsync(form));
        setForm(INITIAL_STATE);
    }

    const inputChange = ev => {
        const { name, value } = ev.target;
        setForm({ ...form, [name]: value });
    };

    return (
        <div className="chat">
            <div >
                <ul className="chat__li">
                    {chats.map((el, index) => {
                        return (
                            <li key={`${el}-${index}`}>
                                {el.messages.map((message, index) => {
                                    return (
                                        <div className="chat__container" key={message}>
                                            <div className="chat__container-img">
                                                <img src={profile} />
                                            </div>
                                            <div className="chat__container-text">
                                                <p>{el.user&&el.user.name}</p>
                                                <li >{message}</li>
                                            </div>
                                        </div>
                                    )
                                })}
                            </li>
                        );
                    })}
                </ul>
            </div>
            <form onSubmit={submit} encType="multipart/form-data" method="POST" className="chat__form">

                <input type="hidden" name="id" value={form.id = user.chat} onChange={inputChange}  className="form__input"/>

                <input type="text-area" name="message" value={form.message} onChange={inputChange}  className="form__input form__input-area"/>

                <button type="submit" className="btn">Enviar</button>
            </form>

        </div>
    )
}

export default Chat;