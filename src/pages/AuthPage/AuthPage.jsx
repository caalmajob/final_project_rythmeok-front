import { useState } from 'react';
import { LoginForm, RegisterForm } from "../../components";

import './AuthPage.scss';


const AuthPage = () => {
    const [formType, setFormType] = useState(false);

    const handleFormType = () => {
        setFormType(!formType);
    }

    if (formType === false) {
        return (
            <div className="auth">
                <div className="form-type">
                    <span className="form-type__title left light">Login</span>
                    <span className="form-type__title right dark" onClick={handleFormType}>Registro</span>
                </div>
                <div className="form-container">
                    <LoginForm />
                </div>
            </div>
        )
    }

    if (formType === true) {
        return (
            <div className="auth">
                <div className="form-type">
                    <span className="form-type__title left dark" onClick={handleFormType}>Login</span>
                    <span className="form-type__title right light">Registro</span>
                </div>
                <div className="form-container">
                    <RegisterForm />
                </div>
            </div>
        )
    }

}

export default AuthPage;