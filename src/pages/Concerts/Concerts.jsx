import { useState } from 'react';
import { useSelector } from 'react-redux';
import {Link} from 'react-router-dom';

import { ConcertCard } from "../../components";

import './Concerts.scss';

const Concerts = () => {
    const [cardType, setCardType] = useState(false);

    const {wishlist, tickets} = useSelector(state => state.user.user);

    const handleCardType = () => {
        setCardType(!cardType);
    }

    if (cardType === false) {
        return (
            <>
                <div className="concerts">
                    <span className="concerts__title left light">Voy a ir</span>
                    <span className="concerts__title right dark" onClick={handleCardType}>Me interesa</span>
                </div>
                <div>
                    <ul className="concerts__card-container">
                        {tickets.length && tickets.map((el, index) => {
                            return (
                                <Link className="concerts__link" to="/ticketdetail"><li className="concerts__list" key={`${el}-${index}`}>
                                <ConcertCard ticket={el}/>
                            </li></Link>);
                        })}
                        {!tickets.length && <p className="concerts__warning">¡Aún no tienes ningún concierto!</p>}
                    </ul>
                </div>
            </>
        )
    }

    if (cardType === true) {
        return (
            <>
                <div className="concerts">
                    <span className="concerts__title left dark" onClick={handleCardType}>Voy a ir</span>
                    <span className="concerts__title right light">Me interesa</span>
                </div>
                <div>
                    <ul className="concerts__card-container">
                        {wishlist.length && wishlist.map((el, index) => {
                            return(
                            <li className="concerts__list" key={`${el}-${index}`}>
                                <ConcertCard ticket={el}/>
                            </li>);
                        })}
                        {!wishlist.length && <p className="concerts__warning">¡Aún no tienes ningún concierto!</p>}
                    </ul>
                </div>
            </>
        )
    }

}

export default Concerts;