import { useEffect } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { Link } from 'react-router-dom';
import { getAllConcertsAsync } from '../../redux/slices/concert.slice';
import {
    Button,
    ConcertList,
} from '../../components';
import './Tickets.scss';


const Tickets = () => {
    const concerts = useSelector(state => state.concerts.all);
    const dispatch = useDispatch();

    useEffect(() => {
        if (!concerts.length) dispatch(getAllConcertsAsync());;
    }, []);


    return (
        <div className="tickets">
            <div className="tickets__button">
            <Link to={{pathname :"/chat"}}><Button message="Chat" bgColor="#01918E" textColor="#fff"/></Link>
            <Button message="Amigos" bgColor="#01918E" textColor="#fff"/>
            <Button message="Fan Club" bgColor="#01918E" textColor="#fff"/>
            </div>
            <ul className="tickets__list">
            {concerts.map((el, index) => {
                        return (
                            <li className="tickets__list" key={`${el}-${index}`}>
                                <ConcertList concertlist={el} />
                            </li>
                        );
                    })}
                    </ul>
        </div>

    )
}

export default Tickets;