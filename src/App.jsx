import { useEffect } from 'react';
import { BrowserRouter as Router, Switch, Route, Redirect } from 'react-router-dom';
import { Home, Tickets, LogoPage, LoadingPage, MusicPlatforms, AuthPage, Payment, UserConfig, ConcertDetail, Chat, Concerts, TicketDetail, SearchConcert } from './pages';
import { useDispatch } from 'react-redux';
import { checkSessionAsync } from './redux/slices/user.slice';
import './App.scss';
import { Header, Navbar, History, SecureRoute } from './components';
import { getAllConcertsAsync } from './redux/slices/concert.slice';


const App = () => {

  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(checkSessionAsync());
    dispatch(getAllConcertsAsync());
  }, []);

  return (
    <Router>
    <History/>
      <div className="app">
      <Header />
        <Switch>
          <Route exact path="/" render={() => {
          return (
            <Redirect to="/logo" />
          )}} />
          <Route exact path="/home" component={(props) => <Home {...props} />} />
          <Route exact path="/auth" component={(props) => <AuthPage {...props} />} />
          <SecureRoute exact path="/tickets" component={(props) => <Tickets {...props} />} />
          <SecureRoute exact path="/concertdetail" component={(props)=> <ConcertDetail {...props} />} />
          <Route exact path="/loading" component={(props) => <LoadingPage {...props} />} />
          <Route exact path="/logo" component={(props) => <LogoPage {...props} />} />
          <SecureRoute exact path="/platforms" component={(props) => <MusicPlatforms {...props} />} />
          <SecureRoute exact path="/payment" component={(props) => <Payment {...props} />} />
          <SecureRoute exact path="/config" component={(props) => <UserConfig {...props} />} />
          <SecureRoute exact path="/chat" component={(props) => <Chat {...props} />} />
          <SecureRoute exact path="/concerts" component={(props) => <Concerts {...props} />} />
          <SecureRoute exact path="/ticketdetail" component={(props) => <TicketDetail {...props} />} />
          <SecureRoute exact path="/search" component={(props) => <SearchConcert {...props} />} />
        </Switch>
        <Navbar />
      </div>
    </Router>
  );
}

export default App;
