import clock from '../../assets/icons/clock.png';
import euro from '../../assets/icons/euro.png';

import './ConcertCard.scss';

const ConcertCard = (props) => {

    const date = new Date(props.ticket.date);
    const formatDate = date.toLocaleDateString();


    return (
        <>
        <div className="concert-card">
            <div className="concert-card__image-container">
                <img src={props.ticket.image} alt="ticket" srcset="" className="concert-card__image" />
            </div>
            <div className="concert-card__info">
                <div className="concert-card__title">{props.ticket.artist}</div>
                <div className="concert-card__date">{formatDate}</div>
                <div className="concert-card__time">
                    <span>
                        <img src={clock} alt="clock" srcset="" className="concert-card__icon" />
                    </span>
                    22:00
                </div>
                <div className="concert-card__price">
                    <span>
                        <img src={euro} alt="euro" srcset="" className="concert-card__icon" />
                    </span>
                    <span>
                    {props.ticket.price}
                    </span>
                    <span>
                    </span>
                </div>
            </div>
        </div>
        <div>
            <iframe
                className="search__iframe"
                src={props.ticket.spotifyUrl}
                width="300"
                height="80"
                frameborder="0"
                allowtransparency="true"
                allow="encrypted-media">
            </iframe>
        </div>
        <div className="concert-card__users">
            <span className="concert-card__attendees">{props.ticket.attendees.length} asistirán</span>
            <span className="concert-card__attendees">{props.ticket.attendees.length} amigos</span>
        </div>
        <div className="concert-card__line"></div>
        </>
    );
};

export default ConcertCard;
