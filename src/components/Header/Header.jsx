import { withRouter } from 'react-router-dom';
import { useState, useEffect} from 'react';
import {useSelector} from 'react-redux';
import './Header.scss';
import arrow from '../../assets/icons/left-arrow.png';

const routes = {
    'tickets': 'Entradas'
}
const Header = (props) => {

    const reduxHistory = useSelector(state => state.user.history)
    
    const [pageName, setPageName] = useState('Rithme')
    
    const getTitle = () => {
        let name = 'Rithme'
        const path = props.location.pathname.split('/')[1]
        if(routes[path]) {
           name = routes[path] 
        }
        setPageName(name)
    } 

    useEffect(()=>{
        getTitle()
    },[props.location.pathname])

    const isHomeRoute = props.location.pathname === '/'
    return(
        <header className="header">
        {!isHomeRoute && reduxHistory !== '' && <span onClick={()=>props.history.goBack()}><img src={arrow} className="arrow"></img></span>}
        <span>{pageName}</span></header>
    )
}

export default withRouter(Header);