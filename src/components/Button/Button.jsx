import './Button.scss';


const Button = ({message, bgColor, textColor}) => {

    return (
        <button className="button" style={{"backgroundColor": bgColor, "color":textColor}}>{message}</button>
    )
}

export default Button;