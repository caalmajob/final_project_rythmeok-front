import { NavLink } from 'react-router-dom';
import './Navbar.scss';
import tickets from '../../assets/icons/tickets.png';
import search from '../../assets/icons/search.png';
import home from '../../assets/icons/star.png';
import concerts from '../../assets/icons/my-concerts.png';
import config from '../../assets/icons/config.png';

const Navbar = () => {
    return(
        <nav className="navbar">
            <ul className="navbar__list">
                <li className="list__element">
                    <NavLink exact to="/tickets" className="link" activeClassName="active">
                        <div>
                            <img src={tickets} className="icon"></img>
                            <span>Comprar</span>
                        </div>
                    </NavLink>
                </li>
                <li className="list__element">
                    <NavLink exact to='/search' className="link" activeClassName="active">
                        <div>
                            <img src={search} className="icon"></img>
                            <span>Buscar</span>
                        </div>
                    </NavLink>
                </li>
                <li className="list__element">
                    <NavLink exact to="/home" className="link" activeClassName="active">
                        <div>
                            <img src={home} className="icon"></img>
                            <span>Home</span>
                        </div>
                    </NavLink>
                </li>
                <li className="list__element">
                    <NavLink exact to='/concerts' className="link" activeClassName="active">
                        <div>
                            <img src={concerts} className="icon"></img>
                            <span>Conciertos</span>
                        </div>
                    </NavLink>
                </li>
                <li className="list__element">
                    <NavLink exact to='/config' className="link" activeClassName="active">
                        <div>
                            <img src={config} className="icon"></img>
                            <span>Ajustes</span>
                        </div>
                    </NavLink>
                </li>
            </ul>
        </nav>
    )
}

export default Navbar
