import { useState, useEffect } from 'react';
import { Redirect } from 'react-router-dom';
import { useDispatch, useSelector } from 'react-redux';
import { checkSessionAsync } from '../../redux/slices/user.slice';
import { registerAsync } from '../../redux/slices/user.slice';
import { setValidation } from '../../redux/slices/user.slice';
import isEmpty from 'validator/lib/isEmpty';
import isLength from 'validator/lib/isLength';
import isStrongPassword from 'validator/lib/isStrongPassword';
import isEmail from 'validator/lib/isEmail';

import './RegisterForm.scss';

const INITIAL_STATE = {
    name: '',
    email: '',
    password: '',
};

const RegisterForm = () => {
    const [formFields, setFormFields] = useState(INITIAL_STATE);
    const [submitted, setSubmitted] = useState(false);
    const {error, validations, user} = useSelector(state => state.user);
    const dispatch = useDispatch();

    const handleFormSubmit = async (ev) => {
        ev.preventDefault();
        dispatch(registerAsync(formFields));
        setFormFields(INITIAL_STATE);
    };

    useEffect(() => {
        if(!user) dispatch(checkSessionAsync());
        if (user)setSubmitted(!submitted);
    }, [user]);

    if (submitted) {
        return <Redirect to='/home' />
    }

    const handleInputChange = (ev) => {
        const { name, value } = ev.target;

        setFormFields({ ...formFields, [name]: value });

    };

    const validateName = (ev, minLength) => {
        const { name, value } = ev.target;
        let message = '';

        if (isEmpty(value)) {
            message += 'El campo no puede estar vacío.';
        }

        if (minLength && !isLength(value, { min: minLength })) {
            message += ` Longitud mínima ${minLength} carácteres`;
        }

        dispatch(setValidation({name, message}));
    };

    const validateEmail = (ev) => {
        const { name, value } = ev.target;
        let message = '';

        if (isEmpty(value)) {
            message += 'El campo no puede estar vacío.';
        }
        if (!isEmail(value)) {
            message += "Please enter valid email address.";
        }

        dispatch(setValidation({name, message}));
    };

    const validatePassword = (ev) => {
        const { name, value } = ev.target;
        let message = '';

        if (isEmpty(value)) {
            message += 'El campo no puede estar vacío.';
        }

        if (!isStrongPassword(value, {
            minLength: 8, minLowercase: 1,
            minUppercase: 1, minNumbers: 1, minSymbols: 1
        })) {
            message += 'La contraseña debe contener 8 caracteres: letras en mayus y minus, números y símbolos.';
        }

        dispatch(setValidation({name, message}));
    };


    return (
        <div className="register-form">
            <form
                className="register-form__field"
                onSubmit={handleFormSubmit}
                method='POST'>
                <label className="register-form__field" htmlFor="name">
                    <input
                        type="text"
                        onBlur={(ev) => validateName(ev, 3)}
                        name="name"
                        id="name"
                        placeholder="Nombre de usuario"
                        onChange={handleInputChange}
                        value={formFields.name}
                    />
                </label>
                {validations['name'] && <div className="register-form__error">{validations['name']}</div>}

                <label className="register-form__field" htmlFor="email">
                    <input
                        type="email"
                        onBlur={(ev) => validateEmail(ev)}
                        name="email"
                        id="email"
                        placeholder="Email"
                        onChange={handleInputChange}
                        value={formFields.email}
                    />
                </label>
                {validations['email'] && <div className="register-form__error">{validations['email']}</div>}

                <label className="register-form__field" htmlFor="password">
                    <input
                        type="password"
                        onBlur={(ev) => validatePassword(ev)}
                        name="password"
                        id="password"
                        placeholder="Contraseña"
                        onChange={handleInputChange}
                        value={formFields.password}
                    />
                </label>
                {validations['password'] && <div className="register-form__error">{validations['password']}</div>}

                <div>
                    <button className="register-form__button" type="submit">Enviar</button>
                </div>
            </form>

            {error && <div className="register-form__error">
                {error}
            </div>}
        </div>
    )
}

export default RegisterForm;