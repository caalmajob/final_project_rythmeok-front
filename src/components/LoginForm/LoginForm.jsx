import { useState } from 'react';
import { Redirect } from 'react-router-dom';
import { useDispatch, useSelector } from 'react-redux';
import { loginAsync } from '../../redux/slices/user.slice';
import './LoginForm.scss';

const INITIAL_STATE = {
    email: '',
    password: '',
}

const LoginForm = (props) => {
    const [formFields, setFormFields] = useState(INITIAL_STATE);
    const [submitted, setSubmitted] = useState(false);
    const {error} = useSelector(state => state.user);
    const dispatch = useDispatch();

    const handleFormSubmit = async ev => {
        ev.preventDefault();
        await dispatch(loginAsync(formFields));
        setSubmitted(!submitted);
    };

    if (submitted) {
        return <Redirect to='/home' />
    }

    const handleInputChange = ev => {
        const { name, value } = ev.target;
        setFormFields({ ...formFields, [name]: value });
    }

    return (
        <div className="login-form">
            <form
                className="login-form__field"
                onSubmit={handleFormSubmit}
                method='POST'>

                <label className="login-form__field" htmlFor="email">
                    <input
                        type="email"
                        name="email"
                        id="email"
                        placeholder="Email"
                        onChange={handleInputChange}
                        value={formFields.email}
                    />
                </label>

                <label className="login-form__field" htmlFor="password">
                    <input
                        type="password"
                        name="password"
                        id="password"
                        placeholder="Contraseña"
                        onChange={handleInputChange}
                        value={formFields.password}
                    />
                </label>

                <div>
                    <button className="login-form__button" type="submit">Entrar</button>
                </div>
            </form>

            {error && <div className="login-form__error">
                {error}
            </div>}
        </div>
    )
}

export default LoginForm;