import { Link } from "react-router-dom";
import './ConcertList.scss';



const ConcertList = (props) => {

    return (
        <div className="concertlist">
            <div className="concertlist__image">
                <img className="concertlist__image-sm" src={props.concertlist.image} />
            </div>

            <div className="concertlist__text">

                <div className="concertlist__header color">
                    <h3 className="aa">{props.concertlist.concertHall.name} </h3>
                    <p className="">{props.concertlist.location}</p>
                </div>

                <div className="concertlist__middle color">
                    <p className="artist">{props.concertlist.artist}</p>
                    <button className="concertlist__middle-button color"><Link to={{ pathname: "/concertdetail", state: {
                        concertInfo: props
                    } }} >Comprar</Link></button>
                </div>

                <div className="concertlist__footer color">
                    <p className="">{props.concertlist.styles}</p>
                    {/* <p className="">{props.concertlist.attendees}</p> */}
                    <p className="">€{props.concertlist.price}</p>
                </div>

            </div>
        </div>

    )
}

export default ConcertList;