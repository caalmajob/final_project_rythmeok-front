import React from 'react';
import { Redirect, Route } from 'react-router-dom';
import { useSelector } from 'react-redux';

const SecureRoute = (props) => {
    const {hasUser} = useSelector(state => state.user);

    if(hasUser === null) {
        return (<div>Cargando ...</div>);
    }

    if(hasUser) {
        return (<Route {...props} />);
    }

    if(!hasUser) {
        return (<Redirect to="/auth" />);
    }
};

export default SecureRoute;

// To use SecureRoute :
// 1. import the component in App: import { SecureRoute } from './components';
// 2. Replace Route with SecureRoute.

// In the target component:
// 1. Import useSelector: import { useSelector } from 'react-redux';
// 2. Declare: const {hasUser} = useSelector(state => state.user);
// 3. if (hasUser) {
//     return (<div>...</div>);
// }