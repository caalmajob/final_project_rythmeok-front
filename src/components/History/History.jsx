import { useEffect, useState} from 'react';
import {useDispatch} from 'react-redux';
import {withRouter} from 'react-router-dom';
import {updateHistory} from '../../redux/slices/user.slice';



const History = (props) =>{
    const [isFirstLoad, setIsFirstLoad] = useState(true);
    const dispatch = useDispatch();
    useEffect(() => {
        if(!isFirstLoad){
            dispatch(updateHistory(props.location.pathname))
        } else {
            setIsFirstLoad(false);
        }
    }, [props.location.pathname])
    return(
        <></>
        )
}

export default withRouter(History);