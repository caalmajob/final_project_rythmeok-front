import { Swiper, SwiperSlide } from 'swiper/react';


import 'swiper/swiper.scss';
import './Gallery.scss';


const Gallery = (props) => {
    const { dataArtist, dataHall, styles } = props;

    return (
        <div className="gallery">
            <Swiper
                spaceBetween={50}
                slidesPerView={4}
            >
                {props.dataArtist &&
                    props.dataArtist.map((item, index) => {
                        return (
                            <SwiperSlide key={`${index}`}>
                                <div className="slide">
                                    <img src={item.image} className="cover" alt={item.name}></img>
                                </div>
                            </SwiperSlide>
                        );
                    })}
                {props.dataHall &&
                    props.dataHall.map((item, index) => {
                        return (
                            <SwiperSlide key={`${index}`}>
                                <div className="slide">
                                    <img src={item.image} className="cover" alt={item.name}></img>
                                </div>
                            </SwiperSlide>
                        );
                    })}
                {styles &&
                    styles.map((style, index) => {
                        return (
                            <SwiperSlide key={`${index}`}>
                                <div className="slide">
                                    <img src={style.url} className="cover" alt={style.name}></img>
                                </div>
                            </SwiperSlide>
                        );
                    })}
            </Swiper>
        </div>
    );
};

export default Gallery;
