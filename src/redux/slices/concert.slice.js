import { createSlice, createAsyncThunk } from '@reduxjs/toolkit';
import { getAllConcerts } from '../../api/concert.api';

const INITIAL_STATE = {
    all: [],
};

export const getAllConcertsAsync = createAsyncThunk('concert/getAll', async () => {
    return await getAllConcerts();
})

export const concertSlice = createSlice({
    name: 'concerts',
    initialState: INITIAL_STATE,
    reducers: {},
    extraReducers: (builder) => {
        builder.addCase(getAllConcertsAsync.fulfilled, (state, action) => {
            state.all = action.payload;
        });
    }
});
