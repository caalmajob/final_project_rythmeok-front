import { createSlice, createAsyncThunk, current } from "@reduxjs/toolkit";
import { register, checkSession, login, logout, getUserData } from "../../api/auth.api";

export const checkSessionAsync = createAsyncThunk("user/checkSession", async () => {
    return await checkSession();
  });

export const loginAsync = createAsyncThunk("user/login", async (form) => {
    return await login(form);
  });

export const registerAsync = createAsyncThunk("user/register", async (form) => {
  return await register(form);
});

export const logoutAsync = createAsyncThunk("user/logout", async () => {
  return await logout();
});

const INITIAL_STATE = {
  user: null,
  hasUser: null,
  error: "",
  validations: {},
  history: '',
  userData: {},
};

export const userSlice = createSlice({
  name: "user",
  initialState: INITIAL_STATE,
  reducers: {
    setValidation: (state, action) => {
        const {name, message } = action.payload;
        state.validations[name] = message;
    },
    updateHistory: (state, action) => {
      state.history = action.payload;
    },
    logoutUser: async (state, action) => {
      const actualState = current(state);
  },
  },
  extraReducers: (builder) => {
    builder.addCase(logoutAsync.fulfilled, (state, action) => {
      state.user = null;
      state.hasUser = null;
      state.error = "";
    });
    builder.addCase(registerAsync.fulfilled, (state, action) => {
      if (!action.payload.message) {
        state.user = action.payload;
        state.hasUser = true;
        state.error = "";
      } else {
        state.hasUser = false;
        state.error = action.payload.message;
      }
    });
    builder.addCase(checkSessionAsync.fulfilled, (state, action) => {
        if (!action.payload.message) {
          state.user = action.payload;
          state.hasUser = true;
          state.error = "";
        } else {
          state.hasUser = false;
        }
    });
    builder.addCase(loginAsync.fulfilled, (state, action) => {
        if (!action.payload.message) {
          state.user = action.payload;
          state.hasUser = true;
          state.error = "";
        } else {
          state.hasUser = false;
          state.error = action.payload.message;
        }
    });
  },
});

export const {
    setValidation,
    updateHistory,
    logoutUser,
} = userSlice.actions;
