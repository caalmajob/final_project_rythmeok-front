import { createSlice, createAsyncThunk } from '@reduxjs/toolkit';
import { getAllConcertHall } from '../../api/concertHall.api';

const INITIAL_STATE = { 
    all: [],
}

export const getAllConcertHallAsync = createAsyncThunk('concertHalls/getAll', async () => {
    return await getAllConcertHall();
})

export const concertHallSlice = createSlice({
    name: 'concertHalls',
    initialState: INITIAL_STATE,
    reducers: {},
    extraReducers: (builder) => {
        builder.addCase(getAllConcertHallAsync.fulfilled, (state, action) => {
            state.all = action.payload;
        });
    }
});