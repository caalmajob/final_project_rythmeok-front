import { createSlice, createAsyncThunk } from '@reduxjs/toolkit';
import { getAllChats, addMessage } from '../../api/chat.api';

const INITIAL_STATE = {
    all: [],
};

export const getAllChatsAsync = createAsyncThunk('chat/getAll', async () => {
    return await getAllChats();
})

export const addMessageAsync = createAsyncThunk('chat/addMessage', async (message) => {
    return await addMessage(message);
})

export const chatSlice = createSlice({
    name: 'chats',
    initialState: INITIAL_STATE,
    reducers: {},
    extraReducers: (builder) => {
        builder.addCase(getAllChatsAsync.fulfilled, (state, action) => {
            state.all = action.payload;
        });
        builder.addCase(addMessageAsync.fulfilled, (state, action) => {
            console.log(action.payload)
            state.all = action.payload;
        });
    }
});

