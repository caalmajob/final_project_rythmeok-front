import { createSlice, createAsyncThunk } from '@reduxjs/toolkit';
import { getAllArtists, getAllStyles, getToken } from '../../api/spotify.api';

const INITIAL_STATE = {
    allStyles: [],
    allArtists: [],
    token: '',
}

export const getAllStylesAsync = createAsyncThunk('styles/getAll', async () => await getAllStyles())

export const getAllArtistsAsync = createAsyncThunk('artists/getAll', async () => await getAllArtists())

export const getTokenAsync = createAsyncThunk('token', async ()=> await getToken());


export const spotifySlice = createSlice({
    name: 'spotify',
    initialState: INITIAL_STATE,
    reducers: {},
    extraReducers: (builder) => {
        builder.addCase(getAllStylesAsync.fulfilled, (state,action)=>{
            state.allStyles = action.payload;
        });
        builder.addCase(getAllArtistsAsync.fulfilled, (state, action)=>{
            state.allArtists = action.payload
        })
        builder.addCase(getTokenAsync.fulfilled, (state, action) => {
            state.token = action.payload
        })
    }
})