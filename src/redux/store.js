import { configureStore } from '@reduxjs/toolkit';
import { userSlice } from './slices/user.slice';
import { concertSlice } from './slices/concert.slice';
import { chatSlice } from './slices/chat.slice';

import { concertHallSlice } from './slices/concertHall.slice';
import { spotifySlice } from './slices/spotify.slice';

export default configureStore({
  reducer: {
    user: userSlice.reducer,
    concerts: concertSlice.reducer,
    chats: chatSlice.reducer,
    concertHalls: concertHallSlice.reducer,
    spotify: spotifySlice.reducer,
  },
});