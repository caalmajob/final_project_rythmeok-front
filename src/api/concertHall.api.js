const getAllConcertHallUrl = "http://localhost:4000/concerthall";

export const getAllConcertHall = async () => {
    const request = await fetch(getAllConcertHallUrl, {
        method: "GET",
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
            'Access-Control-Allow-Origin': '*'
        },
    });

    const response = await request.json();

    return response;
};