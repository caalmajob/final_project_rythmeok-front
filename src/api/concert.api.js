const getAllConcertsUrl = "http://localhost:4000/concert";
const buyConcertUrl = "http://localhost:4000/concert/buyconcert";
const wishlistConcertUrl = "http://localhost:4000/concert/wishlist";


export const getAllConcerts = async () => {
    const request = await fetch(getAllConcertsUrl, {
        method: "GET",
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
            'Access-Control-Allow-Origin': '*',
        },
    });

    const response = await request.json();

    return response;
};



export const buyConcert = async (message) => {
    const request = await fetch(buyConcertUrl, {
        method: 'POST',
        headers: {
            'Accept': 'application/JSON',
            'Content-Type': 'application/json',
            'Access-Control-Allow-Origin': '*',
        },
        body: JSON.stringify(message),
    });

    const response = await request.json();

    return response;

};

export const wishlistConcert = async (message) => {
    const request = await fetch(wishlistConcertUrl, {
        method: 'POST',
        headers: {
            'Accept': 'application/JSON',
            'Content-Type': 'application/json',
            'Access-Control-Allow-Origin': '*',
        },
        body: JSON.stringify(message),
    });

    const response = await request.json();


    return response;

};