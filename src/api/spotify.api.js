const tokenUrl = 'https://accounts.spotify.com/api/token';
const artistsUrl = 'https://api.spotify.com/v1/artists?ids=2CIMQHirSU0MQqyYHq0eOx%2C57dN52uHvrHOxijzpIgu3E%2C1vCWHaC5f2uS3yhpwWbIA6';
const stylesUrl = '	https://api.spotify.com/v1/browse/categories?locale=sv_ES'

const clientId = '2cc80e3d30464705a1e7cb05fd5371c8';
const clientSecret= 'f7c0d70293f4432da0000b86a228afff';



export const getToken = async () => {
    

    const result = await fetch(tokenUrl, {
        method : 'POST',
        headers: {
            'Content-Type': 'application/x-www-form-urlencoded',
            'Authorization': 'Basic ' + btoa(clientId + ':' + clientSecret)
        },
        body: new URLSearchParams({
            grant_type: 'client_credentials',
        })
    });

    const data = await result.json();
    return data.access_token
}

export const getAllStyles = async () => {
    const result = await fetch(stylesUrl, {
        method: 'GET',
        headers: {'Authorization' : 'Bearer ' + await getToken()}
    })

    const data = await result.json();
    return data
}
export const getAllArtists = async () => {
    const result = await fetch(artistsUrl, {
        method: 'GET',
        headers: {'Authorization' : 'Bearer ' + await getToken()}
    })

    const data = await result.json();
    return data
}