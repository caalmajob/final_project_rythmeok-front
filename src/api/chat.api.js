const getAllChatsUrl = "http://localhost:4000/chat";
const addChatUrl = "http://localhost:4000/chat/add";

export const addMessage = async (message) => {
    const request = await fetch(addChatUrl, {
        method: 'POST',
        headers: {
            'Accept': 'application/JSON',
            'Content-Type': 'application/json',
            'Access-Control-Allow-Origin': '*',
        },
        body: JSON.stringify(message),
    });

    const response = await request.json();


    return response;

};


export const getAllChats = async () => {
    const request = await fetch(getAllChatsUrl, {
        method: "GET",
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
            'Access-Control-Allow-Origin': '*',
        },
    });

    const response = await request.json();

    return response;
};

